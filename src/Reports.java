/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author ADAM
 */
public class Reports extends javax.swing.JFrame {

    private HUD hud;
    Statement s = null;
    int MissionID = 5;
    int midID = 0;

    /**
     * Creates new form MissionStats
     */
    public Reports(HUD hud) {
        initComponents();
        this.hud = hud;

        /**
         * Connection to DB to pull mission results
         */
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl = "jdbc:sqlserver://195.195.128.214;" + "databaseName=db_1020421_MarsExplorer; user=user_db_1020421_MarsExplorer; password=P@55word;";
            Connection con = DriverManager.getConnection(connectionUrl);
            s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String UserName = hud.txtUsername.getText();

            midID = midID + MissionID;

            System.out.println(UserName);
            ResultSet rs1 = s.executeQuery("SELECT * FROM Users INNER JOIN Outcomes ON Users.pid = Outcomes.pid WHERE Users.username = '" + UserName + "' AND Outcomes.mid = '" + midID + "'");
            rs1.next();
            /**
             * User details
             */
            txtPilot.setText(UserName);
            txtTestRuns.setText(Integer.toString(rs1.getInt("testRunsMade")));
            txtSuccessful.setText(Integer.toString(rs1.getInt("successfulRuns")));
            txtUnsuccessful.setText(Integer.toString(rs1.getInt("unsuccessfulRuns")));
            /**
             * Mission Details
             */
            txtTimeOutput.setText(rs1.getString("daynight"));
            txtTempOutput.setText(rs1.getString("atmostemp"));
            txtSurfaceOutput.setText(rs1.getString("surfacetemp"));
            txtWeatherOutput.setText(rs1.getString("weather"));
            txtRadiationOutput.setText(rs1.getString("radiation"));

            txtEmergency.setText(rs1.getString("emergency"));

            txtMissionResult.setText(rs1.getString("outcome"));

            txtPower.setText(rs1.getString("power"));
            txtFuel.setText(rs1.getString("fuel"));
            txtHull.setText(rs1.getString("hull"));
            txtShield.setText(rs1.getString("shield"));
            txtPilotNotes.setText(rs1.getString("notes"));


            txtMissionID.setText(Integer.toString(rs1.getInt("mid")));
            txtTimeStamp.setText(rs1.getString("timedate"));
            /**
             * Mission Success / Failure
             */
            if (rs1.getInt("success") == 1 && rs1.getInt("failure") != 1) {


                txtMissionOutcome.setText("The mission was a success!");
            } else if (rs1.getInt("success") != 1 && rs1.getInt("failure") == 1) {


                txtMissionOutcome.setText("The mission was a failure!");
            }

            /**
             * Mission image
             */
            if ("Hull integrity failed and the vessel exploded!".equals(rs1.getString("outcome"))) {

                Icon hullDeath = new ImageIcon(getClass().getResource("Images/HullDeath.png"));
                lblMissionVid.setIcon(hullDeath);
            }




            if ("You crashed into an asteroid!".equals(rs1.getString("outcome"))) {

                Icon asteroidDeath = new ImageIcon(getClass().getResource("Images/AsteroidDeath.png"));
                lblMissionVid.setIcon(asteroidDeath);
            }

            if ("Aliens attacked us. There was nothing you could do, they shot down the vessel!".equals(rs1.getString("outcome"))) {

                Icon alienDeath = new ImageIcon(getClass().getResource("Images/AlienDeath.png"));
                lblMissionVid.setIcon(alienDeath);
            }



            if ("Computer systems failed and we have lost all communication with the vessel!".equals(rs1.getString("outcome"))) {

                Icon compFail = new ImageIcon(getClass().getResource("Images/CompFail.png"));
                lblMissionVid.setIcon(compFail);
            }


            if ("The landing gear will not lower! The mission is a failure.".equals(rs1.getString("outcome"))) {

                Icon landingGear = new ImageIcon(getClass().getResource("Images/LandingGear.png"));
                lblMissionVid.setIcon(landingGear);
            }

            if ("Landing Unsuccessful.  Due to the extreme cold the landing gear seized up and you crashed!".equals(rs1.getString("outcome"))) {

                Icon coldLanding = new ImageIcon(getClass().getResource("Images/ColdLanding.png"));
                lblMissionVid.setIcon(coldLanding);
            }

            if ("Landing Unsuccessful.  Due to the extreme heat the landing gear seized up and you crashed!".equals(rs1.getString("outcome"))) {

                Icon hotLanding = new ImageIcon(getClass().getResource("Images/HotLanding.png"));
                lblMissionVid.setIcon(hotLanding);
            }

            if ("Landing Successful! The rover has deployed and we are ready to continue the mission!".equals(rs1.getString("outcome"))) {

                Icon successfulLanding = new ImageIcon(getClass().getResource("Images/SuccessfulLanding.png"));
                lblMissionVid.setIcon(successfulLanding);
            }

            if ("There was not enough power left to deploy the rover and get its solar panel operational.".equals(rs1.getString("outcome"))) {

                Icon lowPower = new ImageIcon(getClass().getResource("Images/LowPower.png"));
                lblMissionVid.setIcon(lowPower);
            }

            if ("Landing Unsuccessful.  The parachute failed!".equals(rs1.getString("outcome")) || "Landing Unsuccessful.  The parachute failed because your descent was too fast!".equals(rs1.getString("outcome"))) {

                Icon parachuteFail = new ImageIcon(getClass().getResource("Images/ParachuteFail.png"));
                lblMissionVid.setIcon(parachuteFail);
            }

            if ("You landed outside of the LZ and crashed.".equals(rs1.getString("outcome"))) {

                Icon outsideLZ = new ImageIcon(getClass().getResource("Images/OutsideLZ.png"));
                lblMissionVid.setIcon(outsideLZ);
            }





        } catch (Exception ex) {
            System.err.println("Connection Error!");
            JOptionPane.showMessageDialog(null, "Problem found!" + ex, "Error!", WIDTH);
        }




    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBox1 = new javax.swing.JCheckBox();
        lblMissionVid = new javax.swing.JLabel();
        txtSurfaceOutput = new javax.swing.JTextField();
        txtWeatherOutput = new javax.swing.JTextField();
        txtRadiationOutput = new javax.swing.JTextField();
        txtTempOutput = new javax.swing.JTextField();
        txtTimeOutput = new javax.swing.JTextField();
        txtPilot = new javax.swing.JTextField();
        txtTestRuns = new javax.swing.JTextField();
        txtSuccessful = new javax.swing.JTextField();
        txtUnsuccessful = new javax.swing.JTextField();
        lblPilotName = new javax.swing.JLabel();
        lblTestRuns = new javax.swing.JLabel();
        lblSuccessful = new javax.swing.JLabel();
        lblUnsuccessful = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPilotNotes = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        txtEmergency = new javax.swing.JTextField();
        txtPower = new javax.swing.JTextField();
        lblPower = new javax.swing.JLabel();
        lblFuel = new javax.swing.JLabel();
        txtFuel = new javax.swing.JTextField();
        txtHull = new javax.swing.JTextField();
        lblHull = new javax.swing.JLabel();
        lblShield = new javax.swing.JLabel();
        txtShield = new javax.swing.JTextField();
        txtMissionID = new javax.swing.JTextField();
        txtTimeStamp = new javax.swing.JTextField();
        txtMissionOutcome = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        cmdPrev = new javax.swing.JButton();
        cmdNext = new javax.swing.JButton();
        cmdExit = new javax.swing.JButton();
        txtMissionResult = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtMissionResult1 = new javax.swing.JTextField();

        jCheckBox1.setText("jCheckBox1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Mars Explorer Monitor and Control System - Reports");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMissionVid.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(lblMissionVid, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 12, 621, 190));

        txtSurfaceOutput.setEditable(false);
        getContentPane().add(txtSurfaceOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 306, 621, -1));

        txtWeatherOutput.setEditable(false);
        getContentPane().add(txtWeatherOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 332, 621, -1));

        txtRadiationOutput.setEditable(false);
        getContentPane().add(txtRadiationOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 358, 621, -1));

        txtTempOutput.setEditable(false);
        getContentPane().add(txtTempOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 621, -1));

        txtTimeOutput.setEditable(false);
        getContentPane().add(txtTimeOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 254, 621, -1));

        txtPilot.setEditable(false);
        getContentPane().add(txtPilot, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 228, 146, -1));

        txtTestRuns.setEditable(false);
        getContentPane().add(txtTestRuns, new org.netbeans.lib.awtextra.AbsoluteConstraints(162, 228, 154, -1));

        txtSuccessful.setEditable(false);
        getContentPane().add(txtSuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(322, 228, 162, -1));

        txtUnsuccessful.setEditable(false);
        getContentPane().add(txtUnsuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 228, 141, -1));

        lblPilotName.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblPilotName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPilotName.setText("Pilots Name");
        getContentPane().add(lblPilotName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 208, 146, -1));

        lblTestRuns.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTestRuns.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTestRuns.setText("Test Runs Made");
        getContentPane().add(lblTestRuns, new org.netbeans.lib.awtextra.AbsoluteConstraints(162, 208, 154, -1));

        lblSuccessful.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSuccessful.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSuccessful.setText("Successful");
        getContentPane().add(lblSuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(322, 208, 162, -1));

        lblUnsuccessful.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblUnsuccessful.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUnsuccessful.setText("Unsuccessful");
        getContentPane().add(lblUnsuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 208, 141, -1));

        txtPilotNotes.setColumns(20);
        txtPilotNotes.setEditable(false);
        txtPilotNotes.setRows(5);
        jScrollPane1.setViewportView(txtPilotNotes);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(191, 502, 440, 142));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Pilot Notes");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(191, 482, 440, -1));

        txtEmergency.setEditable(false);
        getContentPane().add(txtEmergency, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 384, 621, -1));

        txtPower.setEditable(false);
        getContentPane().add(txtPower, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 456, 149, -1));

        lblPower.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblPower.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPower.setText("Power");
        getContentPane().add(lblPower, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 436, 149, -1));

        lblFuel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblFuel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFuel.setText("Fuel");
        getContentPane().add(lblFuel, new org.netbeans.lib.awtextra.AbsoluteConstraints(165, 436, 165, -1));

        txtFuel.setEditable(false);
        getContentPane().add(txtFuel, new org.netbeans.lib.awtextra.AbsoluteConstraints(165, 456, 166, -1));

        txtHull.setEditable(false);
        getContentPane().add(txtHull, new org.netbeans.lib.awtextra.AbsoluteConstraints(337, 456, 152, -1));

        lblHull.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHull.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHull.setText("Hull");
        getContentPane().add(lblHull, new org.netbeans.lib.awtextra.AbsoluteConstraints(337, 436, 152, -1));

        lblShield.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblShield.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblShield.setText("Shield");
        getContentPane().add(lblShield, new org.netbeans.lib.awtextra.AbsoluteConstraints(495, 436, 136, -1));

        txtShield.setEditable(false);
        getContentPane().add(txtShield, new org.netbeans.lib.awtextra.AbsoluteConstraints(495, 456, 136, -1));

        txtMissionID.setEditable(false);
        getContentPane().add(txtMissionID, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 522, 170, -1));

        txtTimeStamp.setEditable(false);
        getContentPane().add(txtTimeStamp, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 573, 170, -1));

        txtMissionOutcome.setEditable(false);
        getContentPane().add(txtMissionOutcome, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 624, 170, -1));

        cmdPrev.setText("Previous");
        cmdPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdPrevActionPerformed(evt);
            }
        });

        cmdNext.setText("Next");
        cmdNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdNextActionPerformed(evt);
            }
        });

        cmdExit.setText("Exit");
        cmdExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdExitActionPerformed(evt);
            }
        });

        txtMissionResult.setEditable(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Mission ID");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Time and Date");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Mission Outcome");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtMissionResult, javax.swing.GroupLayout.DEFAULT_SIZE, 621, Short.MAX_VALUE)
                        .addGap(19, 19, 19))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cmdPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(cmdNext, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmdExit, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(jLabel2)))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(411, Short.MAX_VALUE)
                .addComponent(txtMissionResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addComponent(jLabel2)
                .addGap(37, 37, 37)
                .addComponent(jLabel3)
                .addGap(37, 37, 37)
                .addComponent(jLabel4)
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdNext)
                    .addComponent(cmdPrev)
                    .addComponent(cmdExit))
                .addGap(22, 22, 22))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 650, 700));

        txtMissionResult1.setEditable(false);
        getContentPane().add(txtMissionResult1, new org.netbeans.lib.awtextra.AbsoluteConstraints(101, 410, 230, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdPrevActionPerformed
        // TODO add your handling code here:
        midID = midID - 1;

        /**
         * Previous Record
         */
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl = "jdbc:sqlserver://195.195.128.214;" + "databaseName=db_1020421_MarsExplorer; user=user_db_1020421_MarsExplorer; password=P@55word;";
            Connection con = DriverManager.getConnection(connectionUrl);
            s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String UserName = hud.txtUsername.getText();

            System.out.println(UserName);
            ResultSet rs1 = s.executeQuery("SELECT * FROM Users INNER JOIN Outcomes ON Users.pid = Outcomes.pid WHERE Users.username = '" + UserName + "' AND Outcomes.mid = '" + midID + "'");
            rs1.next();


            txtPilot.setText(UserName);
            txtTestRuns.setText(Integer.toString(rs1.getInt("testRunsMade")));
            txtSuccessful.setText(Integer.toString(rs1.getInt("successfulRuns")));
            txtUnsuccessful.setText(Integer.toString(rs1.getInt("unsuccessfulRuns")));

            txtTimeOutput.setText(rs1.getString("daynight"));
            txtTempOutput.setText(rs1.getString("atmostemp"));
            txtSurfaceOutput.setText(rs1.getString("surfacetemp"));
            txtWeatherOutput.setText(rs1.getString("weather"));
            txtRadiationOutput.setText(rs1.getString("radiation"));

            txtEmergency.setText(rs1.getString("emergency"));

            txtMissionResult.setText(rs1.getString("outcome"));

            txtPower.setText(rs1.getString("power"));
            txtFuel.setText(rs1.getString("fuel"));
            txtHull.setText(rs1.getString("hull"));
            txtShield.setText(rs1.getString("shield"));
            txtPilotNotes.setText(rs1.getString("notes"));


            txtMissionID.setText(Integer.toString(rs1.getInt("mid")));
            txtTimeStamp.setText(rs1.getString("timedate"));

            if (rs1.getInt("success") == 1 && rs1.getInt("failure") != 1) {


                txtMissionOutcome.setText("The mission was a success!");
            } else if (rs1.getInt("success") != 1 && rs1.getInt("failure") == 1) {


                txtMissionOutcome.setText("The mission was a failure!");
            }

            if ("Hull integrity failed and the vessel exploded!".equals(rs1.getString("outcome"))) {

                Icon hullDeath = new ImageIcon(getClass().getResource("Images/HullDeath.png"));
                lblMissionVid.setIcon(hullDeath);
            }




            if ("You crashed into an asteroid!".equals(rs1.getString("outcome"))) {

                Icon asteroidDeath = new ImageIcon(getClass().getResource("Images/AsteroidDeath.png"));
                lblMissionVid.setIcon(asteroidDeath);
            }

            if ("Aliens attacked us. There was nothing you could do, they shot down the vessel!".equals(rs1.getString("outcome"))) {

                Icon alienDeath = new ImageIcon(getClass().getResource("Images/AlienDeath.png"));
                lblMissionVid.setIcon(alienDeath);
            }



            if ("Computer systems failed and we have lost all communication with the vessel!".equals(rs1.getString("outcome"))) {

                Icon compFail = new ImageIcon(getClass().getResource("Images/CompFail.png"));
                lblMissionVid.setIcon(compFail);
            }


            if ("The landing gear will not lower! The mission is a failure.".equals(rs1.getString("outcome"))) {

                Icon landingGear = new ImageIcon(getClass().getResource("Images/LandingGear.png"));
                lblMissionVid.setIcon(landingGear);
            }

            if ("Landing Unsuccessful.  Due to the extreme cold the landing gear seized up and you crashed!".equals(rs1.getString("outcome"))) {

                Icon coldLanding = new ImageIcon(getClass().getResource("Images/ColdLanding.png"));
                lblMissionVid.setIcon(coldLanding);
            }

            if ("Landing Unsuccessful.  Due to the extreme heat the landing gear seized up and you crashed!".equals(rs1.getString("outcome"))) {

                Icon hotLanding = new ImageIcon(getClass().getResource("Images/HotLanding.png"));
                lblMissionVid.setIcon(hotLanding);
            }

            if ("Landing Successful! The rover has deployed and we are ready to continue the mission!".equals(rs1.getString("outcome"))) {

                Icon successfulLanding = new ImageIcon(getClass().getResource("Images/SuccessfulLanding.png"));
                lblMissionVid.setIcon(successfulLanding);
            }

            if ("There was not enough power left to deploy the rover and get its solar panel operational.".equals(rs1.getString("outcome"))) {

                Icon lowPower = new ImageIcon(getClass().getResource("Images/LowPower.png"));
                lblMissionVid.setIcon(lowPower);
            }

            if ("Landing Unsuccessful.  The parachute failed!".equals(rs1.getString("outcome")) || "Landing Unsuccessful.  The parachute failed because your descent was too fast!".equals(rs1.getString("outcome"))) {

                Icon parachuteFail = new ImageIcon(getClass().getResource("Images/ParachuteFail.png"));
                lblMissionVid.setIcon(parachuteFail);
            }

            if ("You landed outside of the LZ and crashed.".equals(rs1.getString("outcome"))) {

                Icon outsideLZ = new ImageIcon(getClass().getResource("Images/OutsideLZ.png"));
                lblMissionVid.setIcon(outsideLZ);
            }





        } catch (Exception ex) {
            System.err.println("Connection Error!");
            JOptionPane.showMessageDialog(null, "Problem found!" + ex, "Error!", WIDTH);
        }

            }//GEN-LAST:event_cmdPrevActionPerformed

    private void cmdNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdNextActionPerformed
        // TODO add your handling code here:
        midID = midID + 1;

        /**
         * next Record
         */
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl = "jdbc:sqlserver://195.195.128.214;" + "databaseName=db_1020421_MarsExplorer; user=user_db_1020421_MarsExplorer; password=P@55word;";
            Connection con = DriverManager.getConnection(connectionUrl);
            s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String UserName = hud.txtUsername.getText();

            System.out.println(UserName);
            ResultSet rs1 = s.executeQuery("SELECT * FROM Users INNER JOIN Outcomes ON Users.pid = Outcomes.pid WHERE Users.username = '" + UserName + "' AND Outcomes.mid = '" + midID + "'");
            rs1.next();





            txtPilot.setText(UserName);
            txtTestRuns.setText(Integer.toString(rs1.getInt("testRunsMade")));
            txtSuccessful.setText(Integer.toString(rs1.getInt("successfulRuns")));
            txtUnsuccessful.setText(Integer.toString(rs1.getInt("unsuccessfulRuns")));

            txtTimeOutput.setText(rs1.getString("daynight"));
            txtTempOutput.setText(rs1.getString("atmostemp"));
            txtSurfaceOutput.setText(rs1.getString("surfacetemp"));
            txtWeatherOutput.setText(rs1.getString("weather"));
            txtRadiationOutput.setText(rs1.getString("radiation"));

            txtEmergency.setText(rs1.getString("emergency"));

            txtMissionResult.setText(rs1.getString("outcome"));

            txtPower.setText(rs1.getString("power"));
            txtFuel.setText(rs1.getString("fuel"));
            txtHull.setText(rs1.getString("hull"));
            txtShield.setText(rs1.getString("shield"));
            txtPilotNotes.setText(rs1.getString("notes"));


            txtMissionID.setText(Integer.toString(rs1.getInt("mid")));
            txtTimeStamp.setText(rs1.getString("timedate"));

            if (rs1.getInt("success") == 1 && rs1.getInt("failure") != 1) {


                txtMissionOutcome.setText("The mission was a success!");
            } else if (rs1.getInt("success") != 1 && rs1.getInt("failure") == 1) {


                txtMissionOutcome.setText("The mission was a failure!");
            }

            if ("Hull integrity failed and the vessel exploded!".equals(rs1.getString("outcome"))) {

                Icon hullDeath = new ImageIcon(getClass().getResource("Images/HullDeath.png"));
                lblMissionVid.setIcon(hullDeath);
            }




            if ("You crashed into an asteroid!".equals(rs1.getString("outcome"))) {

                Icon asteroidDeath = new ImageIcon(getClass().getResource("Images/AsteroidDeath.png"));
                lblMissionVid.setIcon(asteroidDeath);
            }

            if ("Aliens attacked us. There was nothing you could do, they shot down the vessel!".equals(rs1.getString("outcome"))) {

                Icon alienDeath = new ImageIcon(getClass().getResource("Images/AlienDeath.png"));
                lblMissionVid.setIcon(alienDeath);
            }



            if ("Computer systems failed and we have lost all communication with the vessel!".equals(rs1.getString("outcome"))) {

                Icon compFail = new ImageIcon(getClass().getResource("Images/CompFail.png"));
                lblMissionVid.setIcon(compFail);
            }


            if ("The landing gear will not lower! The mission is a failure.".equals(rs1.getString("outcome"))) {

                Icon landingGear = new ImageIcon(getClass().getResource("Images/LandingGear.png"));
                lblMissionVid.setIcon(landingGear);
            }

            if ("Landing Unsuccessful.  Due to the extreme cold the landing gear seized up and you crashed!".equals(rs1.getString("outcome"))) {

                Icon coldLanding = new ImageIcon(getClass().getResource("Images/ColdLanding.png"));
                lblMissionVid.setIcon(coldLanding);
            }

            if ("Landing Unsuccessful.  Due to the extreme heat the landing gear seized up and you crashed!".equals(rs1.getString("outcome"))) {

                Icon hotLanding = new ImageIcon(getClass().getResource("Images/HotLanding.png"));
                lblMissionVid.setIcon(hotLanding);
            }

            if ("Landing Successful! The rover has deployed and we are ready to continue the mission!".equals(rs1.getString("outcome"))) {

                Icon successfulLanding = new ImageIcon(getClass().getResource("Images/SuccessfulLanding.png"));
                lblMissionVid.setIcon(successfulLanding);
            }

            if ("There was not enough power left to deploy the rover and get its solar panel operational.".equals(rs1.getString("outcome"))) {

                Icon lowPower = new ImageIcon(getClass().getResource("Images/LowPower.png"));
                lblMissionVid.setIcon(lowPower);
            }

            if ("Landing Unsuccessful.  The parachute failed!".equals(rs1.getString("outcome")) || "Landing Unsuccessful.  The parachute failed because your descent was too fast!".equals(rs1.getString("outcome"))) {

                Icon parachuteFail = new ImageIcon(getClass().getResource("Images/ParachuteFail.png"));
                lblMissionVid.setIcon(parachuteFail);
            }

            if ("You landed outside of the LZ and crashed.".equals(rs1.getString("outcome"))) {

                Icon outsideLZ = new ImageIcon(getClass().getResource("Images/OutsideLZ.png"));
                lblMissionVid.setIcon(outsideLZ);
            }





        } catch (Exception ex) {
            System.err.println("Connection Error!");
            JOptionPane.showMessageDialog(null, "Problem found!" + ex, "Error!", WIDTH);
        }


    }//GEN-LAST:event_cmdNextActionPerformed

    private void cmdExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdExitActionPerformed
        // TODO add your handling code here:
        /**
         * Exit
         */
        dispose();
    }//GEN-LAST:event_cmdExitActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdExit;
    private javax.swing.JButton cmdNext;
    private javax.swing.JButton cmdPrev;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblFuel;
    private javax.swing.JLabel lblHull;
    private javax.swing.JLabel lblMissionVid;
    private javax.swing.JLabel lblPilotName;
    private javax.swing.JLabel lblPower;
    private javax.swing.JLabel lblShield;
    private javax.swing.JLabel lblSuccessful;
    private javax.swing.JLabel lblTestRuns;
    private javax.swing.JLabel lblUnsuccessful;
    public javax.swing.JTextField txtEmergency;
    public javax.swing.JTextField txtFuel;
    public javax.swing.JTextField txtHull;
    public javax.swing.JTextField txtMissionID;
    public javax.swing.JTextField txtMissionOutcome;
    public javax.swing.JTextField txtMissionResult;
    public javax.swing.JTextField txtMissionResult1;
    public javax.swing.JTextField txtPilot;
    private javax.swing.JTextArea txtPilotNotes;
    public javax.swing.JTextField txtPower;
    public javax.swing.JTextField txtRadiationOutput;
    public javax.swing.JTextField txtShield;
    public javax.swing.JTextField txtSuccessful;
    public javax.swing.JTextField txtSurfaceOutput;
    public javax.swing.JTextField txtTempOutput;
    public javax.swing.JTextField txtTestRuns;
    public javax.swing.JTextField txtTimeOutput;
    public javax.swing.JTextField txtTimeStamp;
    public javax.swing.JTextField txtUnsuccessful;
    public javax.swing.JTextField txtWeatherOutput;
    // End of variables declaration//GEN-END:variables
}
