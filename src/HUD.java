/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author ADAM
 */
public class HUD extends javax.swing.JFrame implements Runnable {

    /**
     * Set variables
     */
    Point p = null;
    int x = 0;
    int y = 0;
    int a = 0;
    int b = 0;
    int parachuteOpen = 0;
    int parachuteFail = 0;
    int timeOfDay = 0;
    int atmosTemperature = 0;
    int radiationLevels = 0;
    int weather = 0;
    int surfaceTemp = 0;
    int parachuteFailChance = 0;
    int emergencyChance = 0;
    int emergencyLoc = 0;
    int endEmergencyLoc = 0;
    double fuel = 100.00;
    double power = 100.00;
    double fuelMax = 100;
    double powerMax = 100;
    double hull = 100.00;
    double shield = 100.00;
    double hullMax = 100;
    double shieldMax = 100;
    final int ARROW_UP = 38;
    final int ARROW_DOWN = 40;
    final int ARROW_RIGHT = 39;
    final int ARROW_LEFT = 37;
    int landingLocation = 0;
    int landingZoneXLeft = 0;
    int landingZoneWidth = 0;
    int landingZoneXRight = 0;
    int landingZoneY = 0;
    public int emergencyAvoided;
    public int intSuccessful;
    public String strTime;
    public String strTemp;
    public String strSurface;
    public String strWeather;
    public String strRadiation;
    public String txtUsernameOutput;
    public String txtTestRunsOutput;
    public String txtSuccessfulOutput;
    public String txtUnsuccessfulOutput;
    String currentSpeed = "";
    public String txtEmergency;
    public String txtEmergencyDesc;
    public String txtMissionResult;
    public String txtAction1;
    public String txtAction2;
    public String txtAction3;
    String emergencySet = "";
    Statement s = null;

    /**
     * Creates new form HUD
     */
    public HUD() {


        initComponents();

        /**
         * Set label images
         */
        Icon marsExplorer = new ImageIcon(getClass().getResource("Images/MarsExplorer.png"));
        lblShip.setIcon(marsExplorer);
        Icon radar = new ImageIcon(getClass().getResource("Images/RadarTL.gif"));
        lblRadar.setIcon(radar);
        Icon earthSun = new ImageIcon(getClass().getResource("Images/EarthSun.gif"));
        lblEarthSun.setIcon(earthSun);
        Icon marsLeft = new ImageIcon(getClass().getResource("Images/MarsLandLeft.png"));
        lblMarsLeft.setIcon(marsLeft);
        Icon marsRight = new ImageIcon(getClass().getResource("Images/MarsLandRight.png"));
        lblMarsRight.setIcon(marsRight);
        Icon landingZoneIco = new ImageIcon(getClass().getResource("Images/LandingZone.png"));
        lblLandingZone.setIcon(landingZoneIco);
        Icon marsOrbiter = new ImageIcon(getClass().getResource("Images/MarsOrbiter.png"));
        lblMarsOrbiter.setIcon(marsOrbiter);
        Icon nasaEmblem = new ImageIcon(getClass().getResource("Images/NasaEmblem.png"));
        lblNasaEmblem.setIcon(nasaEmblem);
        Icon dustStorm = new ImageIcon(getClass().getResource("Images/DustStorm.png"));
        lblDustStorm.setIcon(dustStorm);
        Icon computerError1 = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
        lblComputerError1.setIcon(computerError1);
        Icon computerError2 = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
        lblComputerError2.setIcon(computerError2);

        Random r = new Random();
        timeOfDay = r.nextInt(2); /*
         * Create random int out of 2 to set day or night
         */

        atmosTemperature = r.nextInt(10) + 1; /*
         * Create random int out of 10
         */
        surfaceTemp = r.nextInt(10) + 1; /*
         * Create random int out of 10
         */
        weather = r.nextInt(10) + 1; /*
         * Create random int out of 10
         */
        radiationLevels = r.nextInt(10) + 1; /*
         * Create random int out of 10
         */

        p = lblShip.getLocation();

        /*
         * Set initial HUD instrument text
         */
        txtLZY.setText("628");
        txtLZXRight.setText("364");
        txtLZXLeft.setText("300");
        txtFuel.setText("100.00%");
        txtPower.setText("100.00%");
        txtHull.setText("100.00%");
        txtShield.setText("100.00%");
        txtShipX.setText("0");
        txtShipY.setText("0");
        txtSpeedOfShip.setText("0 Metres Per Second");
        txtSurfaceDistance.setText("628,000 Metres");
        txtETA.setText("N/A");
        fuel = 100.00;
        power = 100.00;
        hull = 100.00;
        shield = 100.00;
        /*
         * Set all condition to not visible
         */
        lblDustStorm.setVisible(false);
        lblComputerError1.setVisible(false);
        lblComputerError2.setVisible(false);
        lblNightTime.setVisible(false);

        /*
         * Random chance of emergency
         */
        emergencyChance = r.nextInt(10) + 1;

        /*
         * Emergency happens in random location
         */
        emergencyLoc = r.nextInt(200) + 15;
        /*
         * Set time of day
         */
        if (timeOfDay == 0) {
            timeOfDay = timeOfDay + 1;
            txtTime.setText("Night");
            lblNightTime.setVisible(true);

        } else {
            timeOfDay = 0;
            txtTime.setText("Day");

        }

        /*
         * Set atmos temp
         */
        if (atmosTemperature == 1) {
            txtAtmosTemperature.setText("Too Cold");
        }
        if (atmosTemperature == 2) {

            txtAtmosTemperature.setText("Too Hot");

        }
        if (atmosTemperature != 1 && atmosTemperature != 2) {

            txtAtmosTemperature.setText("Average");

        }
        /*
         * Set surface temp
         */
        if (surfaceTemp == 1) {

            txtSurfaceTemp.setText("Below Freezing");

        }
        if (surfaceTemp == 2) {

            txtSurfaceTemp.setText("Melting Solder");

        }
        if (surfaceTemp != 1 && surfaceTemp != 2) {

            txtSurfaceTemp.setText("Perfect");

        }

        /*
         * Set weather
         */

        if (weather == 1) {

            txtWeather.setText("Dust Storm");
            lblComputerError1.setVisible(false);
            lblComputerError2.setVisible(false);

            lblDustStorm.setVisible(true);

        }
        if (weather == 2) {

            txtWeather.setText("EM Storm");
            lblDustStorm.setVisible(false);
            lblComputerError1.setVisible(true);
            lblComputerError2.setVisible(true);

        }
        if (weather != 1 && weather != 2) {

            txtWeather.setText("Normal");
            lblDustStorm.setVisible(false);
            lblComputerError1.setVisible(false);
            lblComputerError2.setVisible(false);

        }
        /*
         * Set radiation
         */
        if (radiationLevels == 1) {

            txtRadiation.setText("High");

        }
        if (radiationLevels == 2) {

            txtRadiation.setText("Medium");


        }
        if (radiationLevels != 1 && radiationLevels != 2) {

            txtRadiation.setText("Low");


        }



    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNightTime = new javax.swing.JLabel();
        lblComputerError2 = new javax.swing.JLabel();
        lblComputerError1 = new javax.swing.JLabel();
        lblDustStorm = new javax.swing.JLabel();
        panSpace = new javax.swing.JPanel();
        lblShip = new javax.swing.JLabel();
        lblMarsLeft = new javax.swing.JLabel();
        lblMarsOrbiter = new javax.swing.JLabel();
        lblMarsRight = new javax.swing.JLabel();
        lblLandingZone = new javax.swing.JLabel();
        lblEarthSun = new javax.swing.JLabel();
        lblRadar = new javax.swing.JLabel();
        panLayout = new javax.swing.JPanel();
        lblNasaEmblem = new javax.swing.JLabel();
        lblUsername = new javax.swing.JLabel();
        lblTestRuns = new javax.swing.JLabel();
        lblSuccessful = new javax.swing.JLabel();
        lblUnsuccessful = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        txtTestRuns = new javax.swing.JTextField();
        txtSuccessful = new javax.swing.JTextField();
        txtUnsuccessful = new javax.swing.JTextField();
        txtSpeedOfShip = new javax.swing.JTextField();
        txtSurfaceDistance = new javax.swing.JTextField();
        lblDistanceSurface = new javax.swing.JLabel();
        lblLandingETA = new javax.swing.JLabel();
        txtETA = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        lblSpeedOfShip = new javax.swing.JLabel();
        lblInfo = new javax.swing.JLabel();
        lblPlanet = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        cmdStart = new javax.swing.JButton();
        cmdReports = new javax.swing.JButton();
        cmdProfile = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        lblShipCoords = new javax.swing.JLabel();
        txtShipX = new javax.swing.JTextField();
        lblFuelLabel = new javax.swing.JLabel();
        lblShipX = new javax.swing.JLabel();
        txtShipY = new javax.swing.JTextField();
        lblShipY = new javax.swing.JLabel();
        lblLZCoords = new javax.swing.JLabel();
        lblLZXLeft = new javax.swing.JLabel();
        txtLZXLeft = new javax.swing.JTextField();
        lblLZXRight = new javax.swing.JLabel();
        lblHull = new javax.swing.JLabel();
        lblShielding = new javax.swing.JLabel();
        lblPowerLabel = new javax.swing.JLabel();
        txtLZXRight = new javax.swing.JTextField();
        lblLZY = new javax.swing.JLabel();
        txtLZY = new javax.swing.JTextField();
        cmdParachute = new javax.swing.JButton();
        cmdOptions = new javax.swing.JButton();
        lblSpeed = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        lblTemperature = new javax.swing.JLabel();
        lblRadiation = new javax.swing.JLabel();
        lblWeather = new javax.swing.JLabel();
        sldSpeed = new javax.swing.JSlider();
        lblSurface = new javax.swing.JLabel();
        txtTime = new javax.swing.JTextField();
        txtAtmosTemperature = new javax.swing.JTextField();
        txtRadiation = new javax.swing.JTextField();
        txtWeather = new javax.swing.JTextField();
        txtSurfaceTemp = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        cmdWait = new javax.swing.JButton();
        txtFuel = new javax.swing.JTextField();
        txtPower = new javax.swing.JTextField();
        txtHull = new javax.swing.JTextField();
        txtShield = new javax.swing.JTextField();
        lblCamera = new javax.swing.JLabel();
        panRight = new javax.swing.JPanel();

        setTitle("Mars Explorer Monitor and Control System - HUD");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblNightTime.setBackground(new java.awt.Color(0, 0, 0));
        lblNightTime.setOpaque(true);
        getContentPane().add(lblNightTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 490, 730, 170));
        getContentPane().add(lblComputerError2, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 590, 260, 70));
        getContentPane().add(lblComputerError1, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 270, 260, 280));
        getContentPane().add(lblDustStorm, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 560, 730, 100));

        panSpace.setBackground(new java.awt.Color(0, 0, 0));
        panSpace.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                panSpaceKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                panSpaceKeyReleased(evt);
            }
        });
        panSpace.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblShip.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblShip.setRequestFocusEnabled(false);
        lblShip.setVerifyInputWhenFocusTarget(false);
        panSpace.add(lblShip, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 15, 40, 37));

        lblMarsLeft.setBackground(new java.awt.Color(51, 0, 0));
        lblMarsLeft.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        panSpace.add(lblMarsLeft, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 631, 320, 29));
        panSpace.add(lblMarsOrbiter, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 50, 9));

        lblMarsRight.setBackground(new java.awt.Color(51, 0, 0));
        lblMarsRight.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        panSpace.add(lblMarsRight, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 630, 350, 30));

        lblLandingZone.setBackground(new java.awt.Color(102, 0, 0));
        panSpace.add(lblLandingZone, new org.netbeans.lib.awtextra.AbsoluteConstraints(281, 650, 100, 10));
        panSpace.add(lblEarthSun, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 0, 90, 70));
        panSpace.add(lblRadar, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 50, 100, 130));

        getContentPane().add(panSpace, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 0, 730, 660));

        panLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        panLayout.add(lblNasaEmblem, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 129, 138));

        lblUsername.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblUsername.setText("Pilot Name:");
        panLayout.add(lblUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, -1, -1));

        lblTestRuns.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTestRuns.setText("Test Runs Performed:");
        panLayout.add(lblTestRuns, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, -1, -1));

        lblSuccessful.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSuccessful.setText("Successful:");
        panLayout.add(lblSuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 420, -1, -1));

        lblUnsuccessful.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblUnsuccessful.setText("Unsuccessful:");
        panLayout.add(lblUnsuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 470, -1, -1));

        txtUsername.setEditable(false);
        panLayout.add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, 122, -1));

        txtTestRuns.setEditable(false);
        panLayout.add(txtTestRuns, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 390, 122, -1));

        txtSuccessful.setEditable(false);
        panLayout.add(txtSuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 440, 122, -1));

        txtUnsuccessful.setEditable(false);
        panLayout.add(txtUnsuccessful, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 122, -1));

        txtSpeedOfShip.setEditable(false);
        panLayout.add(txtSpeedOfShip, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 370, 240, -1));

        txtSurfaceDistance.setEditable(false);
        panLayout.add(txtSurfaceDistance, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 470, 240, -1));

        lblDistanceSurface.setText("Distance from surface:");
        panLayout.add(lblDistanceSurface, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 450, -1, -1));

        lblLandingETA.setText("Landing ETA:");
        panLayout.add(lblLandingETA, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 500, -1, -1));

        txtETA.setEditable(false);
        panLayout.add(txtETA, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 520, 240, -1));
        panLayout.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 550, 270, 10));
        panLayout.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 270, 260, 10));

        lblSpeedOfShip.setText("Speed of ship:");
        panLayout.add(lblSpeedOfShip, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 350, -1, -1));

        lblInfo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblInfo.setText("Current Information");
        panLayout.add(lblInfo, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 280, -1, -1));

        lblPlanet.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblPlanet.setText("Current planet conditions");
        panLayout.add(lblPlanet, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 70, 150, 20));
        panLayout.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 60, 260, 10));

        cmdStart.setText("Deploy");
        cmdStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdStartActionPerformed(evt);
            }
        });
        panLayout.add(cmdStart, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 540, 130, 30));

        cmdReports.setText("Reports");
        cmdReports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdReportsActionPerformed(evt);
            }
        });
        panLayout.add(cmdReports, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 580, 130, 30));

        cmdProfile.setText("Pilot Profile");
        cmdProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdProfileActionPerformed(evt);
            }
        });
        panLayout.add(cmdProfile, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 620, 130, 30));
        panLayout.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 530, 150, 10));

        lblShipCoords.setText("Ship Co-ordinates");
        panLayout.add(lblShipCoords, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 300, -1, -1));

        txtShipX.setEditable(false);
        panLayout.add(txtShipX, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 320, 50, -1));

        lblFuelLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblFuelLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFuelLabel.setText("Fuel");
        panLayout.add(lblFuelLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 600, 50, -1));

        lblShipX.setText("x");
        panLayout.add(lblShipX, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 320, -1, -1));

        txtShipY.setEditable(false);
        panLayout.add(txtShipY, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 320, 50, -1));

        lblShipY.setText("y");
        panLayout.add(lblShipY, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 320, -1, -1));

        lblLZCoords.setText("LZ Co-ordinates");
        panLayout.add(lblLZCoords, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 400, -1, -1));

        lblLZXLeft.setText("x-Left");
        panLayout.add(lblLZXLeft, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 420, 30, -1));

        txtLZXLeft.setEditable(false);
        txtLZXLeft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLZXLeftActionPerformed(evt);
            }
        });
        panLayout.add(txtLZXLeft, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 420, 50, -1));

        lblLZXRight.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLZXRight.setText("x-Right");
        panLayout.add(lblLZXRight, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 420, 40, -1));

        lblHull.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHull.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHull.setText("Hull");
        panLayout.add(lblHull, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 630, 50, -1));

        lblShielding.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblShielding.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblShielding.setText("Sheld");
        panLayout.add(lblShielding, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 630, 50, -1));

        lblPowerLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblPowerLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPowerLabel.setText("Power");
        panLayout.add(lblPowerLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 600, 50, -1));

        txtLZXRight.setEditable(false);
        panLayout.add(txtLZXRight, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 420, 50, -1));

        lblLZY.setText("y");
        panLayout.add(lblLZY, new org.netbeans.lib.awtextra.AbsoluteConstraints(1080, 420, -1, -1));

        txtLZY.setEditable(false);
        panLayout.add(txtLZY, new org.netbeans.lib.awtextra.AbsoluteConstraints(1090, 420, 40, -1));

        cmdParachute.setText("OPEN PARACHUTE");
        cmdParachute.setEnabled(false);
        cmdParachute.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdParachuteActionPerformed(evt);
            }
        });
        panLayout.add(cmdParachute, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 557, 240, -1));

        cmdOptions.setText("Options");
        cmdOptions.setEnabled(false);
        cmdOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdOptionsActionPerformed(evt);
            }
        });
        panLayout.add(cmdOptions, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 240, 110, -1));

        lblSpeed.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSpeed.setText("Descent Speed");
        panLayout.add(lblSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 0, 90, -1));
        lblSpeed.getAccessibleContext().setAccessibleName("Current atmospheric resistance");

        lblTime.setText("Day/Night");
        panLayout.add(lblTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 90, -1, -1));

        lblTemperature.setText("Atmos. Temp.:");
        panLayout.add(lblTemperature, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 120, -1, -1));

        lblRadiation.setText("Radation:");
        panLayout.add(lblRadiation, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 210, -1, -1));

        lblWeather.setText("Weather:");
        panLayout.add(lblWeather, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 180, -1, -1));

        sldSpeed.setMajorTickSpacing(1);
        sldSpeed.setMaximum(5);
        sldSpeed.setMinimum(1);
        sldSpeed.setMinorTickSpacing(1);
        sldSpeed.setPaintLabels(true);
        sldSpeed.setPaintTicks(true);
        sldSpeed.setSnapToTicks(true);
        sldSpeed.setValue(2);
        sldSpeed.setFocusable(false);
        panLayout.add(sldSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 15, 240, -1));

        lblSurface.setText("Surface Temp.:");
        panLayout.add(lblSurface, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 150, -1, -1));

        txtTime.setEditable(false);
        panLayout.add(txtTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 90, 150, -1));

        txtAtmosTemperature.setEditable(false);
        panLayout.add(txtAtmosTemperature, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 120, 150, -1));

        txtRadiation.setEditable(false);
        panLayout.add(txtRadiation, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 210, 150, -1));

        txtWeather.setEditable(false);
        panLayout.add(txtWeather, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 180, 150, -1));

        txtSurfaceTemp.setEditable(false);
        panLayout.add(txtSurfaceTemp, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 150, 150, -1));
        panLayout.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 590, 260, 10));

        cmdWait.setText("Wait 12 Hours");
        cmdWait.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdWaitActionPerformed(evt);
            }
        });
        panLayout.add(cmdWait, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 240, 120, -1));

        txtFuel.setEditable(false);
        panLayout.add(txtFuel, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 600, 60, -1));

        txtPower.setEditable(false);
        panLayout.add(txtPower, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 600, 60, -1));

        txtHull.setEditable(false);
        panLayout.add(txtHull, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 630, 60, -1));

        txtShield.setEditable(false);
        panLayout.add(txtShield, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 630, 60, -1));

        lblCamera.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        panLayout.add(lblCamera, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 130, 170));

        getContentPane().add(panLayout, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1140, 660));

        javax.swing.GroupLayout panRightLayout = new javax.swing.GroupLayout(panRight);
        panRight.setLayout(panRightLayout);
        panRightLayout.setHorizontalGroup(
            panRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 240, Short.MAX_VALUE)
        );
        panRightLayout.setVerticalGroup(
            panRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 660, Short.MAX_VALUE)
        );

        getContentPane().add(panRight, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 0, 240, 660));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdStartActionPerformed
        // TODO add your handling code here:
        /*
         * Deploy ship
         */
        Thread t1 = new Thread(this);
        t1.start();
        panSpace.requestFocus();
        parachuteOpen = 0;
        Icon marsExplorer = new ImageIcon(getClass().getResource("Images/MarsExplorer.png"));
        lblShip.setIcon(marsExplorer);
        cmdWait.setEnabled(false);
        cmdOptions.setEnabled(false);
        sldSpeed.setEnabled(true);
        x = 10;
        y = 0;


    }//GEN-LAST:event_cmdStartActionPerformed

    private void panSpaceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panSpaceKeyPressed
        // TODO add your handling code here:
        /*
         * Move ship
         */
        switch (evt.getKeyCode()) {
            case ARROW_UP:

                int upSpeed = sldSpeed.getValue() - 1;
                sldSpeed.setValue(upSpeed);
                break;

            case ARROW_DOWN:
                int downSpeed = sldSpeed.getValue() + 1;
                sldSpeed.setValue(downSpeed);
                break;

            case ARROW_RIGHT:
                if (fuel > 0) {
                    Icon marsExplorerRight = new ImageIcon(getClass().getResource("Images/MarsExplorerRight.png"));
                    lblShip.setIcon(marsExplorerRight);
                    x = x + 10;
                    fuel = fuel - 1;
                    power = power - 0.5;



                } else if (fuel < 0) {
                    fuel = 0;
                }
                txtFuel.setText(Double.toString(fuel) + "%");
                txtPower.setText(Double.toString(power) + "%");
                break;
            case ARROW_LEFT:
                if (fuel > 0) {
                    Icon marsExplorerLeft = new ImageIcon(getClass().getResource("Images/MarsExplorerLeft.png"));
                    lblShip.setIcon(marsExplorerLeft);
                    x = x - 10;
                    fuel = fuel - 1;
                    power = power - 0.5;



                } else if (fuel < 0) {
                    fuel = 0;
                }
                txtFuel.setText(Double.toString(fuel) + "%");
                txtPower.setText(Double.toString(power) + "%");
                break;
        }




    }//GEN-LAST:event_panSpaceKeyPressed

    private void cmdReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdReportsActionPerformed
        // TODO add your handling code here:
        /*
         * Open reports form
         */
        Reports r = new Reports(this) {
        };
        this.getLocation(p);
        r.setLocation((int) p.getX() + 100, (int) p.getY());
        r.setVisible(true);
    }//GEN-LAST:event_cmdReportsActionPerformed

    private void cmdProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdProfileActionPerformed
        // TODO add your handling code here:
        /*
         * Open profile form
         */
        Profile pro = new Profile(this) {
        };
        this.getLocation(p);
        pro.setLocation((int) p.getX() + 100, (int) p.getY() + 100);
        pro.setVisible(true);
    }//GEN-LAST:event_cmdProfileActionPerformed

    private void panSpaceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panSpaceKeyReleased
        // TODO add your handling code here:
        /*
         * When arrow key released do this
         */
        switch (evt.getKeyCode()) {
            case ARROW_UP:
                Icon marsExplorerR1 = new ImageIcon(getClass().getResource("Images/MarsExplorer.png"));
                lblShip.setIcon(marsExplorerR1);

                break;
            case ARROW_DOWN:

                break;
            case ARROW_RIGHT:
                Icon marsExplorerR2 = new ImageIcon(getClass().getResource("Images/MarsExplorer.png"));
                lblShip.setIcon(marsExplorerR2);
                break;
            case ARROW_LEFT:
                Icon marsExplorerR3 = new ImageIcon(getClass().getResource("Images/MarsExplorer.png"));
                lblShip.setIcon(marsExplorerR3);
                break;
        }

    }//GEN-LAST:event_panSpaceKeyReleased

    private void txtLZXLeftActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLZXLeftActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLZXLeftActionPerformed

    private void cmdParachuteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdParachuteActionPerformed
        // TODO add your handling code here:
        /*
         * If parachute button is hit
         */
        parachuteOpen = 1;
        sldSpeed.setEnabled(false);
        Icon marsExplorerParachute = new ImageIcon(getClass().getResource("Images/MarsExplorerParachute.png"));
        lblShip.setIcon(marsExplorerParachute);
        parachuteFailChance = 0 + (int) (Math.random() * 20); /*
         * Random chance that parachute fails
         */

        if (sldSpeed.getValue() >= 3) { /*
             * If speed too much parachute fails
             */
            Icon marsExplorer = new ImageIcon(getClass().getResource("Images/MarsExplorer.png"));
            lblShip.setIcon(marsExplorer);
            parachuteFail = 1;
            sldSpeed.setValue(3);
            sldSpeed.setValue(4);
            sldSpeed.setValue(5);

        } else {
            if (parachuteFailChance == 20) { /*
                 * 5% Chance of parachute failing
                 */
                parachuteFail = 1;
                Icon marsExplorer = new ImageIcon(getClass().getResource("Images/MarsExplorer.png"));
                lblShip.setIcon(marsExplorer);
            }
            sldSpeed.setValue(1);
            txtSpeedOfShip.setText("250 Metres Per Second");
            txtSpeedOfShip.setText("100 Metres Per Second");
            txtSpeedOfShip.setText("50 Metres Per Second");
            txtSpeedOfShip.setText("10 Metres Per Second");
            txtSpeedOfShip.setText("10 Metres Per Second");
            panSpace.requestFocus();
        }

    }//GEN-LAST:event_cmdParachuteActionPerformed

    private void cmdOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdOptionsActionPerformed
        // TODO add your handling code here:
/*
         * Opens options form
         */
        Options o = new Options(this);
        this.getLocation(p);
        o.setLocation((int) p.getX() + 100, (int) p.getY() + 100);
        o.setVisible(true);
    }//GEN-LAST:event_cmdOptionsActionPerformed

    private void cmdWaitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdWaitActionPerformed
        // TODO add your handling code here:
        /*
         * Waits 12 hours and loads new conditions at random
         */

        atmosTemperature = 1 + (int) (Math.random() * 10);
        surfaceTemp = 1 + (int) (Math.random() * 10);
        weather = 1 + (int) (Math.random() * 10);
        radiationLevels = 1 + (int) (Math.random() * 10);

        if (timeOfDay == 0) {
            timeOfDay = timeOfDay + 1;
            txtTime.setText("Night");
            lblNightTime.setVisible(true);

        } else {
            timeOfDay = 0;
            txtTime.setText("Day");
            lblNightTime.setVisible(false);

        }


        if (atmosTemperature == 1) {
            txtAtmosTemperature.setText("Too Cold");
        }
        if (atmosTemperature == 2) {

            txtAtmosTemperature.setText("Too Hot");

        }
        if (atmosTemperature != 1 && atmosTemperature != 2) {

            txtAtmosTemperature.setText("Average");

        }

        if (surfaceTemp == 1) {

            txtSurfaceTemp.setText("Below Freezing");

        }
        if (surfaceTemp == 2) {

            txtSurfaceTemp.setText("Melting Solder");

        }
        if (surfaceTemp != 1 && surfaceTemp != 2) {

            txtSurfaceTemp.setText("Perfect");

        }



        if (weather == 1) {

            txtWeather.setText("Dust Storm");
            lblDustStorm.setVisible(true);
            lblComputerError1.setVisible(false);
            lblComputerError2.setVisible(false);

        }
        if (weather == 2) {

            txtWeather.setText("EM Storm");
            lblDustStorm.setVisible(false);

            lblComputerError1.setVisible(true);
            lblComputerError2.setVisible(true);

        }
        if (weather != 1 && weather != 2) {

            txtWeather.setText("Normal");
            lblDustStorm.setVisible(false);
            lblComputerError1.setVisible(false);
            lblComputerError2.setVisible(false);


        }

        if (radiationLevels == 1) {

            txtRadiation.setText("High");

        }
        if (radiationLevels == 2) {

            txtRadiation.setText("Medium");

        }
        if (radiationLevels != 1 && radiationLevels != 2) {

            txtRadiation.setText("Low");

        }

    }//GEN-LAST:event_cmdWaitActionPerformed

    @Override
    public void run() {
        try {
            panSpace.requestFocus();
            x = (int) p.getX();
            y = (int) p.getY();
            x = 10;/*
             * Set ship location on deploy
             */
            y = 0;
            int shipX = x;
            int shipY = y;
            int surfaceDistance = 0;

            while (y < 630) {/*
                 * While ship is above ground level
                 */

                shipX = x;
                shipY = y;
                if (power != 0) {
                    power = power - 0.1;

                } else if (power == 0) {
                    power = 0;
                }

                txtPower.setText(new DecimalFormat("##.##").format(power) + "%");


                if (shield <= 0) {
                    shield = 0;
                }
                if (hull > 0) {

                    if (atmosTemperature == 1) {
                        if (shield > 0) {
                            hull = hull - 0.5;
                            shield = shield - 1;
                        } else if (shield == 0) {
                            shield = 0;
                            hull = hull - 2;

                        }
                    } else if (atmosTemperature == 2) {

                        if (shield > 0) {
                            hull = hull - 0.5;
                            shield = shield - 1;
                        } else if (shield == 0) {
                            shield = 0;
                            hull = hull - 1.5;

                        }

                    } else if (atmosTemperature != 1 && atmosTemperature != 2) {



                        if (shield > 0) {
                            hull = hull - 0.05;
                            shield = shield - 0.05;
                        } else if (shield == 0) {
                            shield = 0;
                            hull = hull - 1;

                        }


                    }



                    if (radiationLevels == 1) {

                        if (shield > 0) {
                            shield = shield - 1;
                        } else if (shield == 0) {
                            shield = 0;
                            hull = hull - 1;

                        }





                    } else if (radiationLevels == 2) {

                        if (shield > 0) {
                            shield = shield - 0.5;
                        } else if (shield == 0) {
                            shield = 0;
                            hull = hull - 0.5;

                        }


                    } else if (radiationLevels != 1 && radiationLevels != 2) {

                        if (shield > 0) {
                            shield = shield - 0.05;
                        } else if (shield == 0) {
                            shield = 0;
                            hull = hull - 0.15;

                        }



                    }

                } else if (hull < 0) {
                    hull = 0;
                    /*
                     * When hull = 0 ship explodes
                     */
                    txtMissionResult = "Hull integrity failed and the vessel exploded!";
                    sldSpeed.setValue(5);
                    Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                    lblShip.setIcon(marsExplorerCrash);
                    Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                    lblCamera.setIcon(whiteNoise);

                }


                txtShield.setText(new DecimalFormat("##.##").format(shield) + "%");
                txtHull.setText(new DecimalFormat("##.##").format(hull) + "%");

                surfaceDistance = (landingZoneY - shipY) + 629;
                txtSurfaceDistance.setText(surfaceDistance + ",000 Metres");
                currentSpeed = Integer.toString(sldSpeed.getValue());
                if (parachuteOpen != 1) {
                    txtSpeedOfShip.setText(currentSpeed + ",000 Metres per second");
                }
                int ETA = (surfaceDistance / sldSpeed.getValue());
                txtETA.setText(ETA + " Milliseconds");
                landingLocation = (int) lblShip.getX();
                String strShipX = Integer.toString(shipX);
                String strShipY = Integer.toString(shipY);
                txtShipX.setText(strShipX);
                txtShipY.setText(strShipY);
                lblShip.setLocation(x, y);
                y += sldSpeed.getValue();

                /*
                 * Parachute can be openned and thread will check if it has been
                 */
                if (625 > y && y > 550) {
                    if (parachuteOpen == 1) {
                        cmdParachute.setEnabled(false);
                    } else {
                        cmdParachute.setEnabled(true);
                    }

                }
                /*
                 * Changes radar blip location based on location of ship
                 */
                if (y < 315 && x < 332) /*
                 * TOP LEFT
                 */ {
                    Icon radar = new ImageIcon(getClass().getResource("Images/RadarTL.gif"));
                    lblRadar.setIcon(radar);
                }
                if (y < 315 && x > 332) { /*
                     * TOP RIGHT
                     */
                    Icon radar = new ImageIcon(getClass().getResource("Images/RadarTR.gif"));
                    lblRadar.setIcon(radar);
                }
                if (y > 315 && x < 332) { /*
                     * BOTTOM LEFT
                     */
                    Icon radar = new ImageIcon(getClass().getResource("Images/RadarBL.gif"));
                    lblRadar.setIcon(radar);
                }
                if (y > 315 && x > 332) { /*
                     * BOTTOM RIGHT
                     */
                    Icon radar = new ImageIcon(getClass().getResource("Images/RadarBR.gif"));
                    lblRadar.setIcon(radar);
                }



                /*
                 * Camera images
                 */
                if (y < 150 && y > 0) /*
                 * MARS 1
                 */ {
                    if (hull != 0) {
                        Icon mars1 = new ImageIcon(getClass().getResource("Images/Mars1.png"));
                        lblCamera.setIcon(mars1);
                    }
                }
                if (y < 250 && y > 150) { /*
                     * MARS 2
                     */
                    if (hull != 0
                            && emergencyChance != 1 && emergencyChance != 2 && emergencyChance != 3
                            && emergencyChance != 4) {
                        Icon mars2 = new ImageIcon(getClass().getResource("Images/Mars2.png"));
                        lblCamera.setIcon(mars2);
                    }
                }

                if (y < 350 && y > 250) { /*
                     * MARS 3
                     */
                    if (hull != 0 && emergencyChance != 1 && emergencyChance != 2 && emergencyChance != 3
                            && emergencyChance != 4) {
                        Icon mars3 = new ImageIcon(getClass().getResource("Images/Mars3.png"));
                        lblCamera.setIcon(mars3);
                    }
                    if (hull != 0
                            && emergencyChance == 1 || emergencyChance == 2 || emergencyChance == 3 || emergencyChance == 4
                            && emergencyAvoided == 1) {
                        Icon mars3 = new ImageIcon(getClass().getResource("Images/Mars3.png"));
                        lblCamera.setIcon(mars3);
                    } else if (hull != 0
                            && emergencyChance == 1 || emergencyChance == 2 || emergencyChance == 3 || emergencyChance == 4
                            && emergencyAvoided != 1) {
                        Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                        lblCamera.setIcon(whiteNoise);

                    }
                }
                if (y < 450 && y > 350) { /*
                     * MARS 4
                     */
                    if (hull != 0 && emergencyChance != 1 && emergencyChance != 2 && emergencyChance != 3 && emergencyChance != 4) {
                        Icon mars4 = new ImageIcon(getClass().getResource("Images/Mars4.png"));
                        lblCamera.setIcon(mars4);
                    }
                    if (hull != 0 && emergencyChance == 1 || emergencyChance == 2 || emergencyChance == 3 || emergencyChance == 4 && emergencyAvoided == 1) {
                        Icon mars4 = new ImageIcon(getClass().getResource("Images/Mars4.png"));
                        lblCamera.setIcon(mars4);
                    } else if (hull != 0
                            && emergencyChance == 1 || emergencyChance == 2 || emergencyChance == 3 || emergencyChance == 4
                            && emergencyAvoided != 1) {
                        Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                        lblCamera.setIcon(whiteNoise);

                    }
                }
                if (y < 650 && y > 450) { /*
                     * MARS 5
                     */
                    if (hull != 0 && emergencyChance != 1 && emergencyChance != 2 && emergencyChance != 3 && emergencyChance != 4) {
                        Icon mars5 = new ImageIcon(getClass().getResource("Images/Mars5.png"));
                        lblCamera.setIcon(mars5);
                    }
                    if (hull != 0 && emergencyChance == 1 || emergencyChance == 2 || emergencyChance == 3 || emergencyChance == 4 && emergencyAvoided == 1) {
                        Icon mars5 = new ImageIcon(getClass().getResource("Images/Mars5.png"));
                        lblCamera.setIcon(mars5);
                    } else if (hull != 0
                            && emergencyChance == 1 || emergencyChance == 2 || emergencyChance == 3 || emergencyChance == 4
                            && emergencyAvoided != 1) {
                        Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                        lblCamera.setIcon(whiteNoise);

                    }
                }


                /*
                 * Emergencies
                 */

                endEmergencyLoc = emergencyLoc + 2;



                if (y >= emergencyLoc && y <= endEmergencyLoc) { /*
                     * /* Half Way Emergency Chance
                     */
                    if (emergencyChance == 1 || emergencyChance == 5) {


                        txtEmergency = "Asteroid Alert";
                        txtEmergencyDesc = "An asteroid has been spotting on a collision course with the vessel. We have 3 options, all utilising the thrusters with extra fuel; LEFT, DOWN, or RIGHT by 50.  Only one of these options will allow the mission to continue.";

                        txtAction1 = "Move Left";
                        txtAction2 = "Move Down";
                        txtAction3 = "Move Right";

                        Icon asteroidAlert = new ImageIcon(getClass().getResource("Images/AsteroidMars2.png"));
                        lblCamera.setIcon(asteroidAlert);

                        Emergency e = new Emergency(this);
                        this.getLocation(p);
                        e.setLocation((int) p.getX() + 300, (int) p.getY() + 100);
                        e.setVisible(true);
                        try {

                            URL defaultSound = getClass().getResource("Audio/AsteroidCollision.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                            File soundFile = new File(defaultSound.toURI());
                            System.out.println("defaultSound " + defaultSound);  // check the URL!
                            AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                            Clip clip = AudioSystem.getClip();
                            clip.open(ais);
                            clip.start();



                        } catch (Exception ex) {
                        }




                    }
                    if (emergencyChance == 2 || emergencyChance == 6) {

                        txtEmergency = "Alien Alert";
                        txtEmergencyDesc = "Alien vessel have just uncloaked all around us. Unfortuntely, we never prepared for this situation.";

                        txtAction1 = "We're";
                        txtAction2 = "Done";
                        txtAction3 = "For";

                        Icon alienAlert = new ImageIcon(getClass().getResource("Images/AlienMars2.png"));
                        lblCamera.setIcon(alienAlert);

                        Emergency e = new Emergency(this);
                        this.getLocation(p);
                        e.setLocation((int) p.getX() + 300, (int) p.getY() + 100);
                        e.setVisible(true);
                        try {

                            URL defaultSound = getClass().getResource("Audio/AlienAttack.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                            File soundFile = new File(defaultSound.toURI());
                            System.out.println("defaultSound " + defaultSound);  // check the URL!
                            AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                            Clip clip = AudioSystem.getClip();
                            clip.open(ais);
                            clip.start();

                        } catch (Exception ex) {
                        }


                    }
                    if (emergencyChance == 3 || emergencyChance == 7) {

                        txtEmergency = "Computer System Alert";
                        txtEmergencyDesc = "The computer system has stopped working, we have a limited amount of time.  We must find the fault and repair it quickly.  1 of 3 areas contains the fault, we have one chance at this, which one should we check?";

                        txtAction1 = "Check Power System";
                        txtAction2 = "Check Hydraulics System";
                        txtAction3 = "Check Control System";

                        Emergency e = new Emergency(this);
                        this.getLocation(p);
                        e.setLocation((int) p.getX() + 300, (int) p.getY() + 100);
                        e.setVisible(true);

                        try {
                            URL defaultSound = getClass().getResource("Audio/CompSysFail.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                            File soundFile = new File(defaultSound.toURI());
                            System.out.println("defaultSound " + defaultSound);  // check the URL!
                            AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                            Clip clip = AudioSystem.getClip();
                            clip.open(ais);
                            clip.start();
                        } catch (Exception ex) {
                        }

                    }
                    if (emergencyChance == 4 || emergencyChance == 8) {

                        txtEmergency = "Landing Gear Alert";
                        txtEmergencyDesc = "The landing gear has siezed up.  The mission will fail without landing gear.  We have 3 option to get the landing gear working again, only one of them will work.";

                        txtAction1 = "Flood Hydraulic System";
                        txtAction2 = "Restart All Systems";
                        txtAction3 = "Fire All Thrusters";

                        Emergency e = new Emergency(this);
                        this.getLocation(p);
                        e.setLocation((int) p.getX() + 300, (int) p.getY() + 100);
                        e.setVisible(true);
                        try {
                            URL defaultSound = getClass().getResource("Audio/LandingGearSeized.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                            File soundFile = new File(defaultSound.toURI());
                            System.out.println("defaultSound " + defaultSound);  // check the URL!
                            AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                            Clip clip = AudioSystem.getClip();
                            clip.open(ais);
                            clip.start();

                        } catch (Exception ex) {
                        }

                    } else if (emergencyChance != 1 && emergencyChance != 2 && emergencyChance != 3 && emergencyChance != 4 && emergencyChance != 5 && emergencyChance != 6 && emergencyChance != 7 && emergencyChance != 8) {
                        txtEmergency = "No emergencies occured";
                    }
                }
                /*
                 * Emergencies Avoided
                 */
                if (y == 250 && emergencyAvoided == 1) {



                    if (emergencyChance == 1 || emergencyChance == 5) {

                        txtEmergency = "You managed to avoid an asteroid";

                    }
                    if (emergencyChance == 2 || emergencyChance == 6) {

                        txtEmergency = "You cheated. Aliens cannot be avoided!";

                    }
                    if (emergencyChance == 3 || emergencyChance == 7) {

                        txtEmergency = "You fixed the computer system!";
                    }
                    if (emergencyChance == 4 || emergencyChance == 8) {

                        txtEmergency = "The landing gear is operational!";
                    }
                    /*
                     * Emergencies Not Avoided
                     */
                } else if (y >= 250 && emergencyAvoided == 0) {

                    if (emergencyChance == 1 || emergencyChance == 5) {

                        txtEmergency = "Asteroid collision!";
                        txtMissionResult = "You crashed into an asteroid!";
                        sldSpeed.setValue(5);
                        Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                        lblShip.setIcon(marsExplorerCrash);
                        Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                        lblCamera.setIcon(whiteNoise);

                    }
                    if (emergencyChance == 2 || emergencyChance == 6) {

                        txtEmergency = "Alien attack!";
                        txtMissionResult = "Aliens attacked us. There was nothing you could do, they shot down the vessel!";
                        sldSpeed.setValue(5);
                        Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                        lblShip.setIcon(marsExplorerCrash);
                        Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                        lblCamera.setIcon(whiteNoise);


                    }
                    if (emergencyChance == 3 || emergencyChance == 7) {

                        txtEmergency = "Computer system failed!";
                        txtMissionResult = "Computer systems failed and we have lost all communication with the vessel!";
                        sldSpeed.setValue(5);
                        Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                        lblShip.setIcon(marsExplorerCrash);
                        Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                        lblCamera.setIcon(whiteNoise);


                    }
                    if (emergencyChance == 4 || emergencyChance == 8) {

                        txtEmergency = "Landing gear siezed!";
                        txtMissionResult = "The landing gear will not lower! The mission is a failure.";
                        sldSpeed.setValue(5);
                        Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                        lblShip.setIcon(marsExplorerCrash);
                        Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                        lblCamera.setIcon(whiteNoise);

                    }

                }



                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                /*
                 * Ground level
                 */
                try {
                    if (y >= 620) {

                        if (299 < landingLocation && landingLocation < 365) { /*
                             * Landing Inside LZ
                             */

                            if (parachuteFail != 1 && parachuteOpen == 1) {

                                if (surfaceTemp == 1 && shield < 45 && hull < 80) {


                                    Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                    lblShip.setIcon(marsExplorerCrash);
                                    txtMissionResult = "Landing Unsuccessful.  Due to the extreme cold the landing gear seized up and you crashed!";
                                    Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                    lblCamera.setIcon(whiteNoise);

                                } else if (surfaceTemp == 2 && shield < 45 && hull < 80) {

                                    txtMissionResult = "Landing Unsuccessful.  Due to the extreme heat the landing gear seized up and you crashed!";
                                    Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                    lblShip.setIcon(marsExplorerCrash);
                                    Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                    lblCamera.setIcon(whiteNoise);

                                }
                                if (power >= 20) {
                                    /*
                                     * Only successful result
                                     */

                                    txtMissionResult = "Landing Successful! The rover has deployed and we are ready to continue the mission!";
                                    Icon marsExplorerSuccess1 = new ImageIcon(getClass().getResource("Images/MarsExplorerSuccess1.png"));
                                    lblShip.setIcon(marsExplorerSuccess1);
                                    Thread.currentThread().sleep(100);
                                    Icon marsExplorerSuccess2 = new ImageIcon(getClass().getResource("Images/MarsExplorerSuccess2.png"));
                                    lblShip.setIcon(marsExplorerSuccess2);
                                    Thread.currentThread().sleep(100);
                                    Icon marsExplorerSuccess3 = new ImageIcon(getClass().getResource("Images/MarsExplorerSuccess3.png"));
                                    lblShip.setIcon(marsExplorerSuccess3);
                                    intSuccessful = 1;


                                } else if (power < 20) {
                                    txtMissionResult = "There was not enough power left to deploy the rover and get its solar panel operational.";
                                    Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                    lblCamera.setIcon(whiteNoise);
                                }
                            }
                            if (parachuteFail == 1 && sldSpeed.getValue() == 10) {
                                txtMissionResult = "Landing Unsuccessful.  The parachute failed because your descent was too fast!";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);

                            }
                            if (parachuteFail == 1 && sldSpeed.getValue() != 10) {
                                txtMissionResult = "Landing Unsuccessful.  The parachute failed!";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);

                            }
                            if (parachuteOpen == 0) {
                                txtMissionResult = "Landing Unsuccessful.  You didn't open your parachute!";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);

                            }

                        } else { /*
                             * Landing Outside LZ
                             */
                            if (hull == 0) {
                                txtMissionResult = "Hull integrity failed and the vessel exploded!";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);
                            }
                            if (emergencyChance == 1 || emergencyChance == 5 && emergencyAvoided == 0) {
                                txtMissionResult = "You crashed into an asteroid!";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);
                            }
                            if (emergencyChance == 2 || emergencyChance == 6 && emergencyAvoided == 0) {
                                txtMissionResult = "Aliens attacked us. There was nothing you could do, they shot down the vessel!";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);
                            }
                            if (emergencyChance == 3 || emergencyChance == 7 && emergencyAvoided == 0) {
                                txtMissionResult = "Computer systems failed and we have lost all communication with the vessel!";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);
                            }
                            if (emergencyChance == 4 || emergencyChance == 8 && emergencyAvoided == 0) {
                                txtMissionResult = "The landing gear will not lower! The mission is a failure.";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);
                            }
                            if (emergencyChance != 1 && emergencyChance != 2 && emergencyChance != 3 && emergencyChance != 4 && emergencyChance != 5 && emergencyChance != 6 && emergencyChance != 7 && emergencyChance != 8) {
                                txtMissionResult = "You landed outside of the LZ and crashed.";
                                Icon marsExplorerCrash = new ImageIcon(getClass().getResource("Images/MarsExplorerCrash.png"));
                                lblShip.setIcon(marsExplorerCrash);
                                Icon whiteNoise = new ImageIcon(getClass().getResource("Images/WhiteNoise.gif"));
                                lblCamera.setIcon(whiteNoise);
                            }
                        }

                    }


                } catch (Exception e) {
                    System.err.println(e.getMessage());


                }

            }
            /*
             * On thread end open new stats form
             */
            Stats s = new Stats(this);
            this.getLocation(p);
            s.setLocation((int) p.getX() + 300, (int) p.getY());
            s.setVisible(true);
            /*
             * Hide HUD
             */

            dispose();

        } catch (IOException ex) {
            Logger.getLogger(HUD.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton cmdOptions;
    private javax.swing.JButton cmdParachute;
    private javax.swing.JButton cmdProfile;
    private javax.swing.JButton cmdReports;
    private javax.swing.JButton cmdStart;
    private javax.swing.JButton cmdWait;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    public javax.swing.JLabel lblCamera;
    public javax.swing.JLabel lblComputerError1;
    public javax.swing.JLabel lblComputerError2;
    private javax.swing.JLabel lblDistanceSurface;
    public javax.swing.JLabel lblDustStorm;
    private javax.swing.JLabel lblEarthSun;
    private javax.swing.JLabel lblFuelLabel;
    private javax.swing.JLabel lblHull;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JLabel lblLZCoords;
    private javax.swing.JLabel lblLZXLeft;
    private javax.swing.JLabel lblLZXRight;
    private javax.swing.JLabel lblLZY;
    private javax.swing.JLabel lblLandingETA;
    private javax.swing.JLabel lblLandingZone;
    private javax.swing.JLabel lblMarsLeft;
    private javax.swing.JLabel lblMarsOrbiter;
    private javax.swing.JLabel lblMarsRight;
    private javax.swing.JLabel lblNasaEmblem;
    public javax.swing.JLabel lblNightTime;
    private javax.swing.JLabel lblPlanet;
    private javax.swing.JLabel lblPowerLabel;
    private javax.swing.JLabel lblRadar;
    private javax.swing.JLabel lblRadiation;
    private javax.swing.JLabel lblShielding;
    private javax.swing.JLabel lblShip;
    private javax.swing.JLabel lblShipCoords;
    private javax.swing.JLabel lblShipX;
    private javax.swing.JLabel lblShipY;
    private javax.swing.JLabel lblSpeed;
    private javax.swing.JLabel lblSpeedOfShip;
    private javax.swing.JLabel lblSuccessful;
    private javax.swing.JLabel lblSurface;
    private javax.swing.JLabel lblTemperature;
    private javax.swing.JLabel lblTestRuns;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblUnsuccessful;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JLabel lblWeather;
    private javax.swing.JPanel panLayout;
    private javax.swing.JPanel panRight;
    private javax.swing.JPanel panSpace;
    private javax.swing.JSlider sldSpeed;
    public javax.swing.JTextField txtAtmosTemperature;
    private javax.swing.JTextField txtETA;
    public javax.swing.JTextField txtFuel;
    public javax.swing.JTextField txtHull;
    private javax.swing.JTextField txtLZXLeft;
    private javax.swing.JTextField txtLZXRight;
    private javax.swing.JTextField txtLZY;
    public javax.swing.JTextField txtPower;
    public javax.swing.JTextField txtRadiation;
    public javax.swing.JTextField txtShield;
    private javax.swing.JTextField txtShipX;
    private javax.swing.JTextField txtShipY;
    private javax.swing.JTextField txtSpeedOfShip;
    public javax.swing.JTextField txtSuccessful;
    private javax.swing.JTextField txtSurfaceDistance;
    public javax.swing.JTextField txtSurfaceTemp;
    public javax.swing.JTextField txtTestRuns;
    public javax.swing.JTextField txtTime;
    public javax.swing.JTextField txtUnsuccessful;
    public javax.swing.JTextField txtUsername;
    public javax.swing.JTextField txtWeather;
    // End of variables declaration//GEN-END:variables
}
