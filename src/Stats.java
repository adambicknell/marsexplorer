/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author ADAM
 */
public class Stats extends javax.swing.JFrame {

    /**
     * Set variables
     */
    private HUD hud;
    public String txtTimeReport;
    public String txtTempReport;
    public String txtSurfaceReport;
    public String txtWeatherReport;
    public String txtRadiationReport;
    String txtUsernameOutput;
    String txtTestRunsOutput;
    String txtSuccessfulOutput;
    String txtUnsuccessfulOutput;
    String txtEmergencyOutput;
    String txtMissionResultOutput;
    String txtPowerOutput;
    String txtFuelOutput;
    String txtHullOutput;
    String txtShieldOutput;
    String txtDateTime;
    int success = 0;
    int failure = 0;
    int currentTestRunsMade = 0;
    int currentSuccessfulRuns = 0;
    int currentUnsuccessfulRuns = 0;
    int newTestRunsMade = 0;
    int newSuccessfulRuns = 0;
    int newUnsuccessfulRuns = 0;
    Statement s = null;

    /**
     * Creates new form MissionStats
     */
    public Stats(HUD hud) throws IOException {
        initComponents();
        this.hud = hud;

        String filename;

        /**
         * Pilot details
         */
        txtPilot.setText(getPilot());
        txtTestRuns.setText(getTestRuns());
        txtSuccessful.setText(getSuccessful());
        txtUnsuccessful.setText(getUnsuccessful());

        /**
         * Mission details
         */
        txtTimeOutput.setText(getTimeReport());
        txtTempOutput.setText(getTempReport());
        txtSurfaceOutput.setText(getSurfaceReport());
        txtWeatherOutput.setText(getWeatherReport());
        txtRadiationOutput.setText(getRadiationReport());

        txtEmergency.setText(getEmergency());
        txtMissionResult.setText(getMissionResult());

        txtFuel.setText(getFuel());
        txtPower.setText(getPower());

        txtHull.setText(getHull());
        txtShield.setText(getShield());

        /**
         * Success / Failure
         */
        if (hud.intSuccessful == 1) {
            success = 1;
        } else if (hud.intSuccessful != 1) {
            failure = 1;
        }


        /**
         * Audio and Images
         */
        if ("Hull integrity failed and the vessel exploded!".equals(txtMissionResult.getText())) {

            Icon hullDeath = new ImageIcon(getClass().getResource("Images/HullDeath.png"));
            lblMissionVid.setIcon(hullDeath);

            try {

                URL defaultSound = getClass().getResource("Audio/HullDeath.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();

            } catch (Exception ex) {
            }
        }




        if ("You crashed into an asteroid!".equals(txtMissionResult.getText())) {

            Icon asteroidDeath = new ImageIcon(getClass().getResource("Images/AsteroidDeath.png"));
            lblMissionVid.setIcon(asteroidDeath);
            try {

                URL defaultSound = getClass().getResource("Audio/AsteroidDeath.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();


            } catch (Exception ex) {
            }
        }

        if ("Aliens attacked us. There was nothing you could do, they shot down the vessel!".equals(txtMissionResult.getText())) {

            Icon alienDeath = new ImageIcon(getClass().getResource("Images/AlienDeath.png"));
            lblMissionVid.setIcon(alienDeath);
            try {

                URL defaultSound = getClass().getResource("Audio/AlienDeath.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();


            } catch (Exception ex) {
            }
        }



        if ("Computer systems failed and we have lost all communication with the vessel!".equals(txtMissionResult.getText())) {

            Icon compFail = new ImageIcon(getClass().getResource("Images/CompFail.png"));
            lblMissionVid.setIcon(compFail);
            try {

                URL defaultSound = getClass().getResource("Audio/CompFail.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();


            } catch (Exception ex) {
            }
        }


        if ("The landing gear will not lower! The mission is a failure.".equals(txtMissionResult.getText())) {

            Icon landingGear = new ImageIcon(getClass().getResource("Images/LandingGear.png"));
            lblMissionVid.setIcon(landingGear);
            try {

                URL defaultSound = getClass().getResource("Audio/LandingGear.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();


            } catch (Exception ex) {
            }
        }

        if ("Landing Unsuccessful.  Due to the extreme cold the landing gear seized up and you crashed!".equals(txtMissionResult.getText())) {

            Icon coldLanding = new ImageIcon(getClass().getResource("Images/ColdLanding.png"));
            lblMissionVid.setIcon(coldLanding);
            try {

                URL defaultSound = getClass().getResource("Audio/ColdLanding.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();


            } catch (Exception ex) {
            }
        }

        if ("Landing Unsuccessful.  Due to the extreme heat the landing gear seized up and you crashed!".equals(txtMissionResult.getText())) {

            Icon hotLanding = new ImageIcon(getClass().getResource("Images/HotLanding.png"));
            lblMissionVid.setIcon(hotLanding);
            try {

                URL defaultSound = getClass().getResource("Audio/HotLanding.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();
            } catch (Exception ex) {
            }
        }

        if ("Landing Successful! The rover has deployed and we are ready to continue the mission!".equals(txtMissionResult.getText())) {

            Icon successfulLanding = new ImageIcon(getClass().getResource("Images/SuccessfulLanding.png"));
            lblMissionVid.setIcon(successfulLanding);
            try {

                URL defaultSound = getClass().getResource("Audio/SuccessfulLanding.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();
            } catch (Exception ex) {
            }
        }

        if ("There was not enough power left to deploy the rover and get its solar panel operational.".equals(txtMissionResult.getText())) {

            Icon lowPower = new ImageIcon(getClass().getResource("Images/LowPower.png"));
            lblMissionVid.setIcon(lowPower);
            try {

                URL defaultSound = getClass().getResource("Audio/LowPower.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();


            } catch (Exception ex) {
            }
        }

        if ("Landing Unsuccessful.  The parachute failed!".equals(txtMissionResult.getText()) || "Landing Unsuccessful.  The parachute failed because your descent was too fast!".equals(txtMissionResult.getText())) {

            Icon parachuteFail = new ImageIcon(getClass().getResource("Images/ParachuteFail.png"));
            lblMissionVid.setIcon(parachuteFail);
            try {


                URL defaultSound = getClass().getResource("Audio/ParachuteFail.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();

            } catch (Exception ex) {
            }
        }

        if ("You landed outside of the LZ and crashed.".equals(txtMissionResult.getText())) {

            Icon outsideLZ = new ImageIcon(getClass().getResource("Images/OutsideLZ.png"));
            lblMissionVid.setIcon(outsideLZ);
            try {


                URL defaultSound = getClass().getResource("Audio/OutsideLZ.wav");// getClass().getSy.getResource("Images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                System.out.println("defaultSound " + defaultSound);  // check the URL!
                AudioInputStream ais = AudioSystem.getAudioInputStream(defaultSound);
                Clip clip = AudioSystem.getClip();
                clip.open(ais);
                clip.start();
            } catch (Exception ex) {
            }
        }





    }

    /**
     * Get User and Mission Details from HUD for use in Stats
     */
    private String getPilot() {

        txtUsernameOutput = hud.txtUsername.getText();

        return this.txtUsernameOutput;
    }

    private String getTestRuns() {


        txtTestRunsOutput = hud.txtTestRuns.getText();
        return this.txtTestRunsOutput;
    }

    private String getSuccessful() {


        txtSuccessfulOutput = hud.txtSuccessful.getText();
        return this.txtSuccessfulOutput;
    }

    private String getUnsuccessful() {


        txtUnsuccessfulOutput = hud.txtUnsuccessful.getText();
        return this.txtUnsuccessfulOutput;
    }

    private String getTimeReport() {


        if (hud.timeOfDay == 0) {
            txtTimeReport = "It is day time and the landing zone is in sight.";



        } else {
            txtTimeReport = "It is night time and the ground is not visible.";



        }
        return this.txtTimeReport;
    }

    private String getTempReport() {

        if (hud.atmosTemperature == 1) {
            txtTempReport = "The freezing cold temperature caused increased damage to the shielding and hull.";
        }
        if (hud.atmosTemperature == 2) {

            txtTempReport = "The extreme heat temperature caused increased damage to the shielding and hull.";

        }
        if (hud.atmosTemperature != 1 && hud.atmosTemperature != 2) {

            txtTempReport = "The atmosphere temperature was good and caused minimal damage.";

        }
        return this.txtTempReport;
    }

    private String getSurfaceReport() {


        if (hud.surfaceTemp == 1) {

            txtSurfaceReport = "The surface is currently extremely cold. Hull and shielding must remain above 80% and 45% respectively.";

        }
        if (hud.surfaceTemp == 2) {

            txtSurfaceReport = "The surface is currently extremely hot. Hull and shielding must remain above 80% and 45% respectively.";

        }
        if (hud.surfaceTemp != 1 && hud.surfaceTemp != 2) {

            txtSurfaceReport = "Surface condtions look good for an approach.";

        }
        return this.txtSurfaceReport;
    }

    private String getWeatherReport() {
        if (hud.weather == 1) {

            txtWeatherReport = "The planet is currently suffering a dust storm, we are not able to see the landing zone.";


        }
        if (hud.weather == 2) {

            txtWeatherReport = "An electro-magnetic storm has caused a computer system malfunction.";


        }
        if (hud.weather != 1 && hud.weather != 2) {

            txtWeatherReport = "The weather looks good, and should not cause any interferance.";



        }
        return this.txtWeatherReport;
    }

    private String getRadiationReport() {
        if (hud.radiationLevels == 1) {

            txtRadiationReport = "There are high levels of radiation, this will cause quicker degrdation to shielding and hull.";

        }
        if (hud.radiationLevels == 2) {

            txtRadiationReport = "There are medium levels of radiation, this will cause extra degrdation to shielding and hull.";

        }
        if (hud.radiationLevels != 1 && hud.radiationLevels != 2) {

            txtRadiationReport = "There are low levels of radiation, this will cause minimal effects on our shielding and hull.";

        }
        return this.txtRadiationReport;
    }

    private String getEmergency() {

        txtEmergencyOutput = hud.txtEmergency;

        return this.txtEmergencyOutput;
    }

    private String getMissionResult() {

        txtMissionResultOutput = hud.txtMissionResult;

        return this.txtMissionResultOutput;
    }

    private String getPower() {

        txtPowerOutput = hud.txtPower.getText();

        return this.txtPowerOutput;
    }

    private String getFuel() {


        txtFuelOutput = hud.txtFuel.getText();
        return this.txtFuelOutput;
    }

    private String getHull() {


        txtHullOutput = hud.txtHull.getText();
        return this.txtHullOutput;
    }

    private String getShield() {


        txtShieldOutput = hud.txtShield.getText();
        return this.txtShieldOutput;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBox1 = new javax.swing.JCheckBox();
        txtSurfaceOutput = new javax.swing.JTextField();
        txtWeatherOutput = new javax.swing.JTextField();
        txtRadiationOutput = new javax.swing.JTextField();
        txtTempOutput = new javax.swing.JTextField();
        cmdHUD = new javax.swing.JButton();
        txtTimeOutput = new javax.swing.JTextField();
        lblMissionVid = new javax.swing.JLabel();
        txtPilot = new javax.swing.JTextField();
        txtTestRuns = new javax.swing.JTextField();
        txtSuccessful = new javax.swing.JTextField();
        txtUnsuccessful = new javax.swing.JTextField();
        lblPilotName = new javax.swing.JLabel();
        lblTestRuns = new javax.swing.JLabel();
        lblSuccessful = new javax.swing.JLabel();
        lblUnsuccessful = new javax.swing.JLabel();
        txtMissionResult = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPilotNotes = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        txtEmergency = new javax.swing.JTextField();
        txtPower = new javax.swing.JTextField();
        lblPower = new javax.swing.JLabel();
        lblFuel = new javax.swing.JLabel();
        txtFuel = new javax.swing.JTextField();
        txtHull = new javax.swing.JTextField();
        lblHull = new javax.swing.JLabel();
        lblShield = new javax.swing.JLabel();
        txtShield = new javax.swing.JTextField();

        jCheckBox1.setText("jCheckBox1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtSurfaceOutput.setEditable(false);

        txtWeatherOutput.setEditable(false);

        txtRadiationOutput.setEditable(false);

        txtTempOutput.setEditable(false);

        cmdHUD.setText("Send Report");
        cmdHUD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdHUDActionPerformed(evt);
            }
        });

        txtTimeOutput.setEditable(false);

        txtPilot.setEditable(false);

        txtTestRuns.setEditable(false);

        txtSuccessful.setEditable(false);
        txtSuccessful.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSuccessfulActionPerformed(evt);
            }
        });

        txtUnsuccessful.setEditable(false);

        lblPilotName.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblPilotName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPilotName.setText("Pilots Name");

        lblTestRuns.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTestRuns.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTestRuns.setText("Test Runs Made");

        lblSuccessful.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSuccessful.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSuccessful.setText("Successful");

        lblUnsuccessful.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblUnsuccessful.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUnsuccessful.setText("Unsuccessful");

        txtMissionResult.setEditable(false);

        txtPilotNotes.setColumns(20);
        txtPilotNotes.setRows(5);
        jScrollPane1.setViewportView(txtPilotNotes);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Pilot Notes");

        txtEmergency.setEditable(false);

        txtPower.setEditable(false);

        lblPower.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblPower.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPower.setText("Power");

        lblFuel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblFuel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFuel.setText("Fuel");

        txtFuel.setEditable(false);

        txtHull.setEditable(false);
        txtHull.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHullActionPerformed(evt);
            }
        });

        lblHull.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHull.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHull.setText("Hull");

        lblShield.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblShield.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblShield.setText("Shield");

        txtShield.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtMissionResult)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtEmergency)
                    .addComponent(lblMissionVid, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTempOutput, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtTimeOutput, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblPilotName, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                            .addComponent(txtPilot))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTestRuns)
                            .addComponent(lblTestRuns, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSuccessful)
                            .addComponent(lblSuccessful, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblUnsuccessful, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .addComponent(txtUnsuccessful)))
                    .addComponent(cmdHUD, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtSurfaceOutput, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtWeatherOutput)
                    .addComponent(txtRadiationOutput, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtPower)
                            .addComponent(lblPower, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(txtFuel, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblFuel, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtHull)
                            .addComponent(lblHull, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblShield, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                            .addComponent(txtShield))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(lblMissionVid, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(lblPilotName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblSuccessful, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblUnsuccessful, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTestRuns, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPilot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTestRuns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSuccessful, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUnsuccessful, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTimeOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTempOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSurfaceOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtWeatherOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRadiationOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtEmergency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMissionResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblPower, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblHull, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblShield))
                    .addComponent(lblFuel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPower, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFuel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtHull, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtShield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cmdHUD)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdHUDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdHUDActionPerformed
        // TODO add your handling code here:

        /**
         * Connection to DB to Update user info and also insert mission result
         * into DB
         */
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl = "jdbc:sqlserver://195.195.128.214;" + "databaseName=db_1020421_MarsExplorer; user=user_db_1020421_MarsExplorer; password=P@55word;";
            Connection con = DriverManager.getConnection(connectionUrl);
            s = con.createStatement();
            String UserName = hud.txtUsername.getText();

            System.out.println(UserName);
            ResultSet rsStats = s.executeQuery("Select * from Users WHERE username = '" + UserName + "'");
            rsStats.next();



            int PilotID = (rsStats.getInt("pid"));
            currentTestRunsMade = (rsStats.getInt("testRunsMade"));
            currentSuccessfulRuns = (rsStats.getInt("successfulRuns"));
            currentUnsuccessfulRuns = (rsStats.getInt("unsuccessfulRuns"));

            newTestRunsMade = currentTestRunsMade + 1;
            newSuccessfulRuns = currentSuccessfulRuns + success;
            newUnsuccessfulRuns = currentUnsuccessfulRuns + failure;





            String sql = "INSERT INTO Outcomes (pid, daynight, atmostemp, surfacetemp, weather, radiation, emergency, outcome, notes, timedate, success, failure, fuel, power, shield, hull) VALUES ('" + PilotID + "', '" + txtTimeOutput.getText() + "', '" + txtTempOutput.getText() + "', '" + txtSurfaceOutput.getText() + "', '" + txtWeatherOutput.getText() + "', '" + txtRadiationOutput.getText() + "', '" + txtEmergencyOutput + "', '" + txtMissionResult.getText() + "', '" + txtPilotNotes.getText() + "', '" + getTimeStamp() + "', '" + success + "', '" + failure + "', '" + txtFuel.getText() + "', '" + txtPower.getText() + "', '" + txtShield.getText() + "', '" + txtHull.getText() + "')";

            s.executeUpdate(sql);

            System.out.println("Connection Successful!");




            String sql2 = "UPDATE Users "
                    + "SET testRunsMade = '" + newTestRunsMade + "', successfulRuns = '" + newSuccessfulRuns + "', unsuccessfulRuns = '" + newUnsuccessfulRuns + "' "
                    + "WHERE username = '" + UserName + "'";

            s.executeUpdate(sql2);

            System.out.println("Connection Successful!");






        } catch (Exception ex) {
            System.err.println("Connection Error!");
            JOptionPane.showMessageDialog(null, "Problem found!" + ex, "Error!", WIDTH);
        }
        dispose();
    }//GEN-LAST:event_cmdHUDActionPerformed
    /**
     * Creates mission timestamp
     */
    private Timestamp getTimeStamp() {
        java.util.Date date = null;
        java.sql.Timestamp timeStamp = null;
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            java.sql.Date dt = new java.sql.Date(calendar.getTimeInMillis());
            java.sql.Time sqlTime = new java.sql.Time(calendar.getTime().getTime());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            date = simpleDateFormat.parse(dt.toString() + " " + sqlTime.toString());
            timeStamp = new java.sql.Timestamp(date.getTime());
        } catch (ParseException pe) {
        } catch (Exception e) {
        }
        return timeStamp;
    }
    private void txtSuccessfulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSuccessfulActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSuccessfulActionPerformed

    private void txtHullActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHullActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHullActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdHUD;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblFuel;
    private javax.swing.JLabel lblHull;
    private javax.swing.JLabel lblMissionVid;
    private javax.swing.JLabel lblPilotName;
    private javax.swing.JLabel lblPower;
    private javax.swing.JLabel lblShield;
    private javax.swing.JLabel lblSuccessful;
    private javax.swing.JLabel lblTestRuns;
    private javax.swing.JLabel lblUnsuccessful;
    public javax.swing.JTextField txtEmergency;
    public javax.swing.JTextField txtFuel;
    public javax.swing.JTextField txtHull;
    public javax.swing.JTextField txtMissionResult;
    public javax.swing.JTextField txtPilot;
    private javax.swing.JTextArea txtPilotNotes;
    public javax.swing.JTextField txtPower;
    public javax.swing.JTextField txtRadiationOutput;
    public javax.swing.JTextField txtShield;
    public javax.swing.JTextField txtSuccessful;
    public javax.swing.JTextField txtSurfaceOutput;
    public javax.swing.JTextField txtTempOutput;
    public javax.swing.JTextField txtTestRuns;
    public javax.swing.JTextField txtTimeOutput;
    public javax.swing.JTextField txtUnsuccessful;
    public javax.swing.JTextField txtWeatherOutput;
    // End of variables declaration//GEN-END:variables
}
